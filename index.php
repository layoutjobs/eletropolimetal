<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">

		<title>Eletropolimetal</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/main.min.css" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,700,300italic,700italic" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body data-spy="scroll" data-target=".main-header" data-offset="115">

		<div class="main-alerts">
			<!--[if lt IE 9]>
			<div class="alert alert-danger alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
				O seu navegador está muito desatualizado, por favor, atualize seu navegador para melhorar sua experiência. 
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<![endif]-->
		</div>
		
		<header class="main-header navbar navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".main-header-navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#top" data-smooth-scroll><img src="img/logo-eletropolimetal.png" width="341" height="70" alt="Eletropolimetal"></a>
				</div>
				<nav class="main-header-navbar-collapse navbar-collapse collapse" aria-expanded="false">
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden"><a href="#top" data-smooth-scroll>Topo</a></li>
						<li class="first"><a href="#empresa" data-smooth-scroll>Empresa</a></li>
						<li><a href="#eletropolimento" data-smooth-scroll>Eletropolimento</a></li>
						<li><a href="#qualidade" data-smooth-scroll>Qualidade</a></li>
						<li><a href="#clientes" data-smooth-scroll>Clientes</a></li>
						<li class="last"><a href="#contato" data-smooth-scroll>Contato</a></li>
					</ul>
				</nav>
			</div>
		</header>
		
		<section class="main-section main-section-dark text-center" id="top">
			<div class="container">
				<div class="spacing spacing-2 hidden-xs hidden-sm"></div>
				<div class="spacing spacing-1 hidden-xs hidden-sm hidden-lg"></div>
				<h1 class="heading heading-xl">Eletropolimento de peças em inox <span class="spacing visible-md visible-lg"></span> para empresas e indústrias</h1>
				<p class="lead lead-xl">Atendemos indústrias e empresas dos mais <span class="spacing visible-md visible-lg"></span> variados segmentos e mercado.</p>
				<div class="spacing spacing-1"></div>
				<div class="img-group">
					<ul>
						<li class="hidden-xs"><img src="img/eletropolimento-top-01.png" width="160" height="160" alt="Eletropolimento"></li>
						<li><span class="spacing spacing-1"></span><img src="img/eletropolimento-top-02.png" width="160" height="160" alt="Eletropolimento"></li>
						<li><span class="spacing spacing-2"></span><img src="img/eletropolimento-top-03.png" width="160" height="160" alt="Eletropolimento"></li>
						<li><span class="spacing spacing-2"></span><img src="img/eletropolimento-top-04.png" width="160" height="160" alt="Eletropolimento"></li>
						<li><span class="spacing spacing-1"></span><img src="img/eletropolimento-top-05.png" width="160" height="160" alt="Eletropolimento"></li>
						<li class="hidden-xs"><img src="img/eletropolimento-top-06.png" width="160" height="160" alt="Eletropolimento"></li>
					</ul>
				</div>
				<div class="spacing spacing-2 hidden-xs hidden-sm"></div>
				<div class="spacing spacing-1 hidden-xs hidden-sm hidden-lg"></div>
			</div>
		</section>

		<section class="main-section text-center" id="empresa">
			<div class="container">
				<h1 class="heading heading-lg">Quem Somos</h1>
				<div class="spacing spacing-3"></div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<p class="lead">A Eletropolimetal Ltda. ME. está no mercado desde 2012, realizando trabalhos de eletropolimento de peças para indústrias e empresas das mais diversas áreas: médica, hospitalar, siderúrgica, metalúrgica, agrícola, alimentícia, entre outras.</p>
						<div class="spacing spacing-1"></div>
						<p class="lead">Está localizada na cidade de Mogi Mirim, Estado de São Paulo, onde possui toda a estrutura necessária para a realização de eletropolimento de peças em inox, de pequeno a médio porte.</p>
						<div class="spacing spacing-3"></div>
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<div class="img-group img-group-horizontal">
									<ul>
										<li><img src="img/eletropolimento-empresa-01.png" width="220" height="220" alt="Eletropolimento"></li>
										<li><img src="img/eletropolimento-empresa-02.png" width="220" height="220" alt="Eletropolimento"></li>
										<li><img src="img/eletropolimento-empresa-03.png" width="220" height="220" alt="Eletropolimento"></li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section main-section-dark text-center" id="eletropolimento">
			<div class="container">
				<h1 class="heading heading-lg">Eletropolimento</h1>
				<div class="spacing spacing-4"></div>
				<h4 class="heading heading-xs">O que é eletropolimento:</h4>
				<p>Eletropolimento é a remoção eletrolítica do metal em uma solução altamente iônica por meio de um potencial e corrente elétricos. É um meio efetivo para redução de micro-asperezas que caracteriza a superfície de aço inox e metais similares de construção. A superfície de aspecto liso de componentes eletropolidos torna mais difícil o depósito e retenção de impurezas flutuantes. Tal processo é normalmente usado para remover uma camada bem fina de material na superfície de uma peça ou componente de metal.</p>
				<div class="spacing spacing-3"></div>
				<h4 class="heading heading-xs">Como é realizado o eletropolimento:</h4>
				<p>Inicialmente, é realizada a limpeza da peça com desengraxante, com a finalidade de remover qualquer impureza presente na superfície da mesma. A seguir é realizado o eletropolimento. A Eletropolimetal utiliza o método de eletropolimento por banho, onde a peça é imersa em um tanque com solução eletrolítica onde, por passagem de corrente elétrica retificada, ocorre a remoção eletrolítica (eletropolimento).</p>
				<div class="spacing spacing-3"></div>
				<h4 class="heading heading-xs">Benefícios do eletropolimento:</h4>
				<p>Na indústria de transformação todo aço com superfície impregnada está contaminado com "ferro livre", que pode acelerar processos de corrosão. O processo de eletropolimento, além de remover o ferro livre da superfície da peça, ainda promove um significativo nivelamento da mesma; dependendo da composição desta, também pode ocorrer o processo de “passivação”, responsável por formar uma camada protetora na superfície da peça, de grande importância contra a oxidação. O eletropolimento também é uma excelente técnica para limpeza de material na preparação para soldagem, uma vez que remove todos os contaminantes da superfície daquele, além de aliviar a tensão superficial; o que proporciona uma redução na quantidade de escória produzida nas soldas, tornando o trabalho mais fácil, limpo e uniforme. Tal processo também tem a capacidade de melhorar as propriedades do material de uma peça de trabalho, o que se adiciona à mudança de suas dimensões físicas, sendo que esta depende basicamente do próprio metal e como ele foi processado até chegar ao eletropolimento.</p>
			</div>
		</section>

		<section class="main-section text-center" id="qualidade">
			<div class="container">
				<h1 class="heading heading-lg">Qualidade</h1>
				<div class="spacing spacing-3"></div>	
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<p class="lead lead-lg">A Eletropolimetal preza pela qualidade de seus serviços em todos os processos, desde o recebimento do equipamento, até a sua entrega, sempre visando surpreender com o resultado final e com a agilidade.</p>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section main-section-gray text-center" id="clientes">
			<div class="container">
				<div class="spacing spacing-1"></div>
				<h1 class="heading heading-lg">Clientes</h1>
				<div class="spacing spacing-3"></div>
				<p class="lead">Conheça abaixo alguns de nossos clientes. Clique sobre a imagem para obter mais informações.</p>
				<div class="spacing spacing-3"></div>
				<div class="img-group img-group-horizontal">
					<ul>
						<li><a href="http://www.imbil.com.br/" target="_blank"><img src="img/logo-imbil.jpg" width="170" height="90" alt="Imbil"></a></li>
						<li><a href="http://www.buffalogrill.com.br/" target="_blank"><img src="img/logo-buffalogrill.jpg" width="170" height="90" alt="Buffalo Grill"></a></li>
						<li><a href="http://www.baumer.com.br/" target="_blank"><img src="img/logo-baumer.jpg" width="170" height="90" alt="Baumer"></a></li>
						<li><a href="http://www.incomagri.com.br/" target="_blank"><img src="img/logo-incomagri.jpg" width="170" height="90" alt="Incomagri"></a></li>
						<li><a href="http://www.suprir.com.br/" target="_blank"><img src="img/logo-suprir.jpg" width="170" height="90" alt="Suprir"></a></li>
					</ul>
				</div>	
				<div class="spacing spacing-1"></div>
			</div>
		</section>

		<section class="main-section main-section-dark text-center" id="contato">
			<div class="container">
				<h1 class="heading heading-lg">Contato</h1>
				<div class="spacing spacing-3"></div>
				<p class="lead">Entre em contato conosco para mais informações ou para solicitar um orçamento.</p>
				<div class="spacing spacing-2"></div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<form id="contactForm" action="contact.php">
							<div class="row">
								<div class="col-md-4 form-group text-center">
									<label class="sr-only" for="contactFormName">Seu nome</label>
									<input class="form-control input-lg" id="contactFormName" type="text" name="ContactForm[name]" placeholder="Seu nome">
								</div>
								<div class="col-md-4 form-group text-center-xs text-center-sm">
									<label class="sr-only" for="contactFromPhone">Seu e-mail</label>
									<input class="form-control input-lg" id="contactFromPhone" type="text" name="ContactForm[phone]" placeholder="Seu telefone">
								</div>
								<div class="col-md-4 form-group text-center-xs text-center-sm">
									<label class="sr-only" for="contactFromEmail">Seu e-mail</label>
									<input class="form-control input-lg" id="contactFromEmail" type="text" name="ContactForm[email]" placeholder="Seu e-mail">
								</div>
							</div>
							<div class="spacing spacing-col visible-md visible-lg"></div>
							<div class="form-group">
								<label class="sr-only" for="contactFromMessage">Mensagem</label>
								<textarea class="form-control input-lg" id="contactFromMessage" name="ContactForm[message]" placeholder="Mensagem"></textarea>
							</div>
							<div class="spacing spacing-col visible-md visible-lg"></div>
							<div class="form-group">
								<button class="btn btn-lg btn-primary" type="submit">Enviar Mensagem</button>
							</div>
						</form>
					</div>
				</div>
				<div class="spacing spacing-2"></div>
				<address class="lead">Fone Fixo: (19) 3863 .6356 — Fone Móvel: (19) 99665.7475<br>
				Rua São Miguel, 407 — Vila Bianchi — Mogi Mirim/SP<br>
				E-mail: <a href="mailto:eletropolimetal@eletropolimetal.com.br">eletropolimetal@eletropolimetal.com.br</a></address>
			</div>
			<div class="spacing spacing-6"></div>
			<div class="main-map"> 
				<div id="map" class="main-map-map"></div>
				<a class="main-map-link btn" href="https://goo.gl/maps/r0vyS" target="_blank">Visualizar no Google Maps</a>
			</div>
		</section>

		<footer class="main-footer">
			<div class="container">
				Copyright &copy <?php echo date('Y'); ?>. Eletropolimetal. Todos os direitos reservados.
			</div>
		</footer>

		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>